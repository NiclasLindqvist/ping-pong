describe('ping-pong', function() {
  describe('new game view', function() {
    it('has fields for player names', function() {
      browser.get('http://localhost:3000/new');

      expect(element(by.model('new.player1')).isPresent()).toEqual(true);
      expect(element(by.model('new.player2')).isPresent()).toEqual(true);
    });

    it('can start the new game', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function(url) {
          expect(url.indexOf('details') > -1).toBeTruthy();
        });
      });
    });
  });
});