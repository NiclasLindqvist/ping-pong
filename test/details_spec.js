'use strict';
describe('ping-pong', function() {

  describe('details view', function() {

    it('can show game details', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function() {
          element.all(by.repeater('player in details.game.pair track by $index'))
          .then(function(player) {
            expect(player[0].element(by.binding('player')).getText()).toEqual('p1');
            expect(player[1].element(by.binding('player')).getText()).toEqual('p2');
          });
        });
      });
    });

    it('can increase score count', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function() {
          element.all(by.repeater('player in details.game.pair track by $index'))
          .then(function(players) {
          
            var scoreCount;
            players[0].element(by.binding('details.game.scoreTally[player]'))
            .getText()
            .then(function(text){ 
              scoreCount = text;
            });

            players[0].element(by.css('.add-point'))
            .click()
            .then(function () {
              players[0].element(by.binding('details.game.scoreTally[player]'))
              .getText()
              .then(function(text) {
                expect(parseFloat(text)).toEqual((parseFloat(scoreCount) || 0) + 1);
              });

              players[0].element(by.binding('details.game.scoreTally[player]')).getText()
              .then(function(text){ 
                scoreCount = text;
              });
              players[0].element(by.css('.add-point'))
              .click()
              .then(function () {
                element(by.binding('details.game.scoreTally[player]')).getText()
                .then(function (text) {
                  expect(parseFloat(text)).toEqual(parseFloat(scoreCount) + 1);
                })
              });
            });
          });
        });
      });
    });

    it('ends a set when score is over 10', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function() {
          element.all(by.repeater('player in details.game.pair track by $index'))
          .then(function(players) {
            players[0].element(by.className('add-point'))
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .then(function () {
              element(by.binding('details.game.scoreTally[player]'))
              .getText()
              .then(function(text) {
                expect(parseFloat(text)).toEqual(0);
              });
              element(by.binding('details.game.setTally[details.game.pair[0]]'))
              .getText()
              .then(function(text) {
                expect(parseFloat(text)).toEqual(1);
              });
            });
          });
        });
      });
    });

    it('does not end a set when score is over 10 but lead is not two or more', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function() {
          element.all(by.repeater('player in details.game.pair track by $index'))
          .then(function(players) {
            players[1].element(by.className('add-point'))
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click();

            players[0].element(by.className('add-point'))
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .then(function () {
              players[0].element(by.binding('details.game.scoreTally[player]'))
              .getText()
              .then(function(text) {
                expect(parseFloat(text)).toEqual(11);
              });
              element(by.binding('details.game.setTally[details.game.pair[0]]'))
              .getText()
              .then(function(text) {
                expect(parseFloat(text)).toEqual(0);
              });
            });
          });
        });
      });
    });


    it('shows a list of set results', function () {
      browser.get('http://localhost:3000/new');
      element(by.model('new.player1')).sendKeys('p1');
      element(by.model('new.player2')).sendKeys('p2');

      var startButton = element(by.id('start-game'))
      .click()
      .then(function(){
        browser.getCurrentUrl()
        .then(function() {
          element.all(by.repeater('player in details.game.pair track by $index'))
          .then(function(players) {
            players[0].element(by.className('add-point'))
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .click()
            .then(function () {
              element.all(by.repeater('set in details.game.sets'))
              .then(function(sets){
                var firstSet = sets[0].getText();
                expect(firstSet).toEqual('10-0');
              })
            });
          });
        });
      });
    });

  });

});