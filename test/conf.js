exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['home_spec.js', 'new_spec.js', 'details_spec.js'],
  jasmineNodeOpts: {
    showColors: true,
    isVerbose: true,
  },
};