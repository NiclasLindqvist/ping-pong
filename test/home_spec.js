describe('ping-pong', function() {
  describe('home view', function () {

    it('should show a startpage', function() {
      browser.get('http://localhost:3000/');

      expect(browser.getTitle()).toEqual('Ping Pong Score!');
      expect(element(by.css('#title')).getText()).toEqual('Ping Pong Score!');
    });

    it('should provide a away to start new game', function () {
      browser.get('http://localhost:3000/');

      var startButton = element(by.id('new-game')).click();

      var currentUrl = browser.getCurrentUrl();
      expect(currentUrl).toEqual('http://localhost:3000/new');
      expect(element(by.css('#title')).getText()).toEqual('New game');
    });

    // it('should list some stats of wins and losses', function(){
    //   browser.get('http://localhost:3000/new');
    //   element(by.model('new.player1')).sendKeys('p1');
    //   element(by.model('new.player2')).sendKeys('p2');

    //   var startButton = element(by.id('start-game'))
    //   .click()
    //   .then(function(){
    //     browser.getCurrentUrl()
    //     .then(function() {
    //       element.all(by.repeater('player in details.game.pair track by $index'))
    //       .then(function(players) {
          
    //         var scoreCount;
    //         players[0].element(by.binding('details.game.scoreTally[player]'))
    //         .getText()
    //         .then(function(text){ 
    //           scoreCount = text;
    //         });

    //         // Eeh, sorry 'bout this, win a game with a player, 22 points!
    //         players[0].element(by.css('.add-point'))
    //         .click().click().click().click().click().click().click().click().click().click().click()
    //         .click().click().click().click().click().click().click().click().click().click().click()
    //         .then(function () {
    //           browser.get('http://localhost:3000/home');
    //           element.all(by.repeater('(name, player) in index.stats'))
    //           .then(function(stats) {
    //             var firstName = stats[0].element(by.binding('name')).getText();
    //             expect(firstName).toEqual('p1');
    //           });
    //         });
    //       });
    //     });
    //   });      
    // });
  });
});