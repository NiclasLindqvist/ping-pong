define([], function(app) {
  function gameStorage (localStorageService) {

    var storage = {
      new: newGame,
      get: getGame,
      updateScore: updateScore,
      list: listGames,
      listStats: listStats
    }

    return storage;

    // Public

    function newGame (player1, player2) {
      var game, id, gameSaved;
      game = {
        pair: [player1, player2],
        startDate: new Date()
      };

      id = "game_"+guid();
      gameSaved = localStorageService.set(id, game);

      if(gameSaved) {
        return id;
      } else {
        return gameSaved;
      }
    }

    function getGame (id) {
      return localStorageService.get(id);
    }

    function updateScore (id, name, change) {
      var game, gameSaved;
      var game = getGame(id);
      game.scoreTally = game.scoreTally || {};
      game.scoreTally[name] = (game.scoreTally[name] || 0) + change;

      if(setIsOver(game)) {
        game = nextSet(id);
      } 
      
      gameSaved = localStorageService.set(id, game);
      
      if(gameSaved) {
        return game;
      } else {
        return gameSaved;
      }
    }

    function listGames () {
      var gameIds, games;
      gameIds = localStorageService.keys();
      games = [];
      for (var i = gameIds.length - 1; i >= 0; i--) {
        if(gameIds[i].indexOf("game_") > -1) {
          var game = localStorageService.get(gameIds[i]);
          game.id = gameIds[i];
          games.push(game);
        }
      };

      return games;
    }

    function listStats () {
      var stats;
      stats = localStorageService.get("gameStats");
      return stats;
    }

    // Private

    /* If one score is above 10 and at least 2 points higher than opponent */
    function setIsOver (game) {
      var difference, p1, p1_score, p2, p2_score;
      p1 = game.pair[0];
      p2 = game.pair[1];

      p1_score = (game.scoreTally[p1] || 0);
      p2_score = (game.scoreTally[p2] || 0);

      if(p1_score > 10 || p2_score > 10) {
        difference = Math.abs(p1_score - p2_score);
        if(difference >= 2) {
          return true;
        }
      }
      return false;
    }

    function nextSet (id) {
      var game, p1, p2, p1_score, p2_score, setWinner, gameSaved, setLoser;
      game = getGame(id);
      game.setTally = game.setTally || {};

      p1 = game.pair[0];
      p1_score = (game.scoreTally[p1] || 0);

      p2 = game.pair[1];
      p2_score = (game.scoreTally[p2] || 0);

      if(p1_score > p2_score) {
        setWinner = p1;
        setLoser = p2;
      }

      if(p1_score < p2_score) {
        setWinner = p2;
        setLoser = p1;
      }

      game.sets = game.sets || [];
      game.sets.push([p1_score, p2_score]);
      game.setTally[setWinner] = (game.setTally[setWinner] || 0) + 1;
      game.scoreTally = {};

      if(game.setTally[setWinner] >= 2) {
        game.winner = setWinner;
        updateGameStats(setWinner, setLoser);
      };

      return game;
    }

    function updateGameStats (winner, loser) {
      var gameStats, statsSaved;
      gameStats = (localStorageService.get("gameStats") || {});
      gameStats[winner] = (gameStats[winner] || {});
      gameStats[winner].wins = (gameStats[winner].wins || 0 ) + 1;

      gameStats[loser] = (gameStats[loser] || {});
      gameStats[loser].losses = (gameStats[loser].losses || 0 ) + 1;

      statsSaved = localStorageService.set("gameStats", gameStats);

      return statsSaved;
    }

    // http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
  }

  gameStorage.$inject = ['localStorageService'];

  return gameStorage;
});