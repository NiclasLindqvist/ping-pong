define([], function(){
  function config($routeProvider, 
    $locationProvider, 
    localStorageServiceProvider) {
    $routeProvider
      .when('/home', {templateUrl: 'templates/home.html', controller: 'indexController'})
      .when('/new', {templateUrl: 'templates/new.html', controller: 'newController'})
      .when('/details/:id',{templateUrl:'templates/details.html', controller:'detailsController'})
      .otherwise({redirectTo: '/home'});

    $locationProvider
      .html5Mode(true)
      .hashPrefix('!');

    localStorageServiceProvider
      .setPrefix('pingPong')
      .setStorageType('localStorage')
      .setStorageCookie(0, '/')
      .setNotify(true, false);

  }
  config.$inject=[
  '$routeProvider', 
  '$locationProvider', 
  'localStorageServiceProvider'];

  return config;
});
