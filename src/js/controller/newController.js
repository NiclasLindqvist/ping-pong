define([], function() {
  function newController ($scope, $location, gameStorage){
    var newCtrl = this;

    newCtrl.title = 'New game';

    newCtrl.startGame = function () {
      var gameSaved, player1, player2, id;

      player1 = newCtrl.player1;
      player2 = newCtrl.player2;

      if(!player1 || !player2) {
        return false;
      }

      var id = gameStorage.new(player1, player2);
      if(!id) {
        return false;
      } 

      // all went well, move along
      $location.path('/details/' + id);
    }

  }

  newController.$inject=['$scope', '$location', 'gameStorage'];

  return newController;
});
