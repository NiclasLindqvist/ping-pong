define([], function() {
  function detailsController ($scope, $routeParams, gameStorage){
    var detailsCtrl = this;
    detailsCtrl.game = gameStorage.get($routeParams.id);

    detailsCtrl.addPoint = function (name) {
      detailsCtrl.game = gameStorage.updateScore($routeParams.id, name, 1);
    }

    detailsCtrl.removePoint = function (name) {
      detailsCtrl.game = gameStorage.updateScore($routeParams.id, name, -1);
    }
  }

  detailsController.$inject=['$scope', '$routeParams', 'gameStorage'];

  return detailsController;
});
