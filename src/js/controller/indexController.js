define([], function() {
  function indexController ($scope, $location, gameStorage){
    var indexCtrl = this;

    indexCtrl.title = 'Ping-pong';
    indexCtrl.games = gameStorage.list();
    indexCtrl.stats = gameStorage.listStats();
  }

  indexController.$inject=['$scope', '$location', 'gameStorage'];

  return indexController;
});
