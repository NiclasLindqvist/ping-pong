define([
  'src/js/config',
  'src/js/factory/gameStorage',
  'src/js/controller/indexController',
  'src/js/controller/newController',
  'src/js/controller/detailsController'
  ],

  function(config, gameStorage, indexController, newController, detailsController) {
    var app = angular.module('pingPong', [
      'ngRoute',
      'ngResource',
      'LocalStorageModule'
    ]);
    app.config(config);
    app.service('gameStorage', gameStorage);
    app.controller('indexController', indexController);
    app.controller('newController', newController);
    app.controller('detailsController', detailsController);
  }
);
