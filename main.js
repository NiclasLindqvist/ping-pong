require(['src/js/app'],
    function() {
        'use strict';

        angular.bootstrap(document, ['pingPong']);
    }
);