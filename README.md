Ping Pong Score
====

Prerequisits
----

You need to have nodejs, npm and bower installed to get you going. For testing see the testing section of this document.

Install
----

All project specific dependencies should be installed by running these two commands

    npm install
    bower install



Build application and run server
----

Build styles and start a watch for changes of the img and scss folders

    grunt

There are two settings for running the application:

*Unminified output*

    grunt dev

*Minified output*

    grunt release


Testing
----

To test some end to end tests using Protractor and Selenium Web Driver, you'll have to have those tools installed and a web driver instace started (you can change it in the test/conf.js but by default the driver instance is at `http://localhost:4444/wd/hub`)

    npm run test