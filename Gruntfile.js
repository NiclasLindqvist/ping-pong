var path = require('path');

module.exports = function (grunt) {
  // require('jit-grunt')(grunt);

  grunt.initConfig({
    express:{
      dev: {
        options: {
          server: path.resolve('./server'),
          port:3000,
          hostname:'*'
        }
      }
    },
    requirejs: {
      options: {
        paths: {
          'appFiles': './src/js'
        },
        removeCombined: true,
        out: './app/js/app.js',
        optimize: 'none',
        name: 'main'
      },
      dev:{
        options:{
          optimize:'none'
        }
      },
      release:{
        options:{
          optimize:'uglify'
        }
      }
    },
    sass: {
      options: {},
      dist: {
        files: [{
          expand: true,
          cwd: './src/scss',
          src: ['./**/*.scss'],
          dest: './app/css',
          ext: '.css'
        }]
      }
    },
    copy:{
      main: {
        files: [{
          expand: true, 
          cwd: './src/img', 
          src: ['./**/*.png', './**/*.jpg'], 
          dest: './app/img'
        }]
      }
    },
    watch: {
      styles: {
        files: ['src/scss/**/*.scss'],
        tasks: ['sass'],
        options: {
          nospawn: true
        }
      },
      images: {
        files: ['src/img/**/*.jpg', 'src/img/**/*.png'],
        tasks: ['copy'],
        options: {
          nospawn: true
        }
      }
    }

  });
  
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express');

  // grunt.registerTask('default', ['less', 'watch'])
  grunt.registerTask('default', ['sass', 'copy', 'watch'])
  grunt.registerTask('dev',['requirejs:dev','express:dev','express-keepalive']);
  grunt.registerTask('release',['requirejs:release','express:dev','express-keepalive']);
}